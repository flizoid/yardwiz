$ ->
  removeHash = () ->
    loc = window.location
    if "pushState" of history
      history.pushState '', document.title, loc.pathname + loc.search
    else
      scrollV = document.body.scrollTop
      scrollH = document.body.scrollLeft
      loc.hash = ''
      document.body.scrollTop = scrollV
      document.body.scrollLeft = scrollH
  removeHash()
  isMobile =
    Android: ->
      navigator.userAgent.match /Android/i
  
    BlackBerry: ->
      navigator.userAgent.match /BlackBerry/i
  
    iOS: ->
      navigator.userAgent.match /iPhone|iPad|iPod/i
  
    Opera: ->
      navigator.userAgent.match /Opera Mini/i
  
    Windows: ->
      navigator.userAgent.match /IEMobile/i
  
    any: ->
      isMobile.Android() or isMobile.BlackBerry() or isMobile.iOS() or isMobile.Opera() or isMobile.Windows()
  $('a:not(.dont-scroll)').smoothScroll(offset: -70)
  $("#phone").formance 'format_phone_number'
  $("#zip").formance 'format_number'
  $("#email").formance 'format_email'
  $("#state").mask("aa", placeholder: "  ")
  $('.collapse a').click () ->
    $('.navbar-collapse').collapse('hide')
  jQuery.browser = {}
  (->
    jQuery.browser.msie = false
    jQuery.browser.version = 0
    if navigator.userAgent.match(/MSIE ([0-9]+)\./)
      jQuery.browser.msie = true
      jQuery.browser.version = RegExp.$1
  )()
  detectIE = () ->
    if $.browser.msie && $.browser.version == 9
      return true
    if $.browser.msie && $.browser.version == 8
      return true
    return false

  if detectIE() || typeof(window.orientation) != 'undefined'
    $('.animated').css('display': 'block', 'visibility': 'visible')
  else
    $('.scroll.animated').appear()
    $('.scroll.animated').on 'appear', () ->
      el = $(@)
      anim = el.data('anim')
      animationDelay = el.data('animation-delay')
      if animationDelay
        setTimeout( ->
          el.addClass(anim + " visible")
        , animationDelay)
      else
        el.addClass("#{anim} visible")
  $("#prev").click (event) ->
    curr = $('#services .row.content:not(.hidden-xs)')
    prev = curr.prev('.row.content.hidden-xs')
    next = curr.next('.row.content.hidden-xs')
    if prev.length > 0
      curr.toggleClass 'hidden-xs'
      prev.toggleClass 'hidden-xs'
    else if next.length > 0
      curr.toggleClass 'hidden-xs'
      next.toggleClass 'hidden-xs'

  $("#next").click (event) ->
    curr = $('#services .row.content:not(.hidden-xs)')
    next = curr.next('.row.content.hidden-xs')
    prev = curr.prev('.row.content.hidden-xs')
    if next.length > 0
      curr.toggleClass 'hidden-xs'
      next.toggleClass 'hidden-xs'
    else if prev.length > 0
      curr.toggleClass 'hidden-xs'
      prev.toggleClass 'hidden-xs'
  if isMobile.any()
    $("#navy").addClass "navbar-fixed-top"
    $("#services").css "margin-top" : "60px"
    $("#nav-logo").addClass "scrolled"	
  else
    $('body').scrollspy target: '#navi', offset: 70
    height = $(window).height() - $('#navy').outerHeight()
    $('#home').height(height)
    $(window).resize ->
      height = $(window).height() - $('#navy').outerHeight()
      $('#home').height(height)
    $(window).scroll ->
      windowScroll = $(window).scrollTop()
      windowHeight = $(window).height()
      
      navPosition = $("#home").outerHeight()
      if windowScroll > navPosition
        $("#navy").addClass "navbar-fixed-top"
        $("#services").addClass "scrolled"
        $("#nav-logo").addClass "scrolled"
      else if windowScroll < navPosition
        $("#navy").removeClass "navbar-fixed-top"
        $("#services").removeClass "scrolled"
        $("#nav-logo").removeClass "scrolled"