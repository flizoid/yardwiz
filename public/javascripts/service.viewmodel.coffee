$	->
  class ServicesViewModel
    constructor: () ->
      @services = ko.observableArray()
      @selectedService = ko.observable()
      @added_services = ko.observableArray()
      @selectedDropdown = ko.observable()
      @hasErrors = ko.observable(false)
      @succeeded = ko.observable(false)
      @formError = ko.observable()
      @added_services.extend({ notify: 'always' });

      @services.push(new ServiceModel(
        id:"snow-removal"
        name: "Snow Removal"
        included:["Snow Removal", "Salt Placement"]
        options:["Residential","Commercial"]
        usesRadios: true
        selectedOption:"Residential"))  
      @services.push(new ServiceModel(
        id:"lawn-care"
        name: "Lawn Care"
        included:["Lawn Mowing", "Trim Bushes", "Mulch Placement", "Plant Maintence"]
        options:["Residential","Commercial"]
        usesRadios: true
        selectedOption:"Residential"))
      @services.push(new ServiceModel(
        id:"plant-service"
        name: "Plant Service"
        included:["Plant your purchased plants", "Consultation on planting options"]
        options:["Annual Planting","Laying Sod", "Mulch Placement"]))
      @services.push(new ServiceModel(
        id:"tree-service"
        name: "Tree Service"
        included:["Free Estimates"]
        options:["Large Tree Removal","Stump Removal", "Storm Damage & Cleanup","Shrub Maintenance", "Other"]))
      @services.push(new ServiceModel(
        id:"fence-deck"
        name: "Fence & Deck Services"
        included:["Free Estimates", "Design Consultation"]
        options:["Fencing","Decking"]))
      @services.push(new ServiceModel(
        id:"install-design"
        name: "Installation & Design"
        included:["Free Estimates", "Design Consultation"]
        options:["Short Project Description","Other"]))
      @services.push(new ServiceModel(
        id:"trash-removal"
        name: "Yard Trash Removal"
        included:["Free Estimates"]
        options:["Yard Trash Removal"]))
      @services.push(new ServiceModel(
        id:"water-system"
        name: "Irrigation System"
        included:["Free Estimates", "Design Consultation"]
        options:["Irrigation System", "Other"]))
      @selectService = (id) =>
        service = _.find(@services(), (s) => s.id() == id)
        @selectedService(service)
        return true
      @addService = (service) =>
        @added_services(_.reject(@added_services(), (serv)-> service.name() == serv))
        @added_services.push service.name()
        @selectedDropdown(service)
        $('a:not(.dont-scroll)').smoothScroll(offset: -70)
        return true
      @removeService = (name) =>
      	remove = confirm("Are you sure you want to remove #{name}?")
      	if remove
      	 service = _.find(@services(), (s) => s.name() == name)
      	 @added_services.remove(service.name())
      	 if @added_services().length > 0
      		  addThis = _.find(@services(), (s) => s.name() == @added_services()[0])
      		  @selectedDropdown(addThis)
      	 else @selectedDropdown(new ServiceModel())
      	 service.selectedOptions([])
      @updateDropdown = (id) =>
        service = _.find(@services(), (s) => s.name() == id)
        @selectedDropdown(service)
      @toggleDropdown = (data, event) =>
        self = $(event.target)
        $("div.select-styled.active").each ->
          $(@).removeClass("active").next("ul.select-options").hide()
        $(self).toggleClass("active").next("ul.select-options").toggle()
        $(document).off('click.selectDropdown')
        $(document).on('click.selectDropdown', ->
          $("div.select-styled.active").each ->
            $(@).removeClass("active").next("ul.select-options").hide()    
        )
      @selectDropdown = (name, event) =>
        self = $(event.target)
        @updateDropdown(name)
        $("div.select-styled.active").removeClass "active"
        $(self).val $(self).attr("rel")
        self.parent('ul.select-options').hide()
      @submitForm = () =>
        $('.input-wlabel').removeClass("errored")
        result = validateForm()
        @hasErrors(false)
        $('.input-wlabel').children('label').removeClass("errored")
        if !result.success
          @hasErrors(true)
          for field in result.fields
            field.parents('.input-wlabel').children('label').addClass("errored")
          if result.messages.length > 1
            str = result.messages.slice(0, -1).join(', ') + ', and '+ result.messages.slice(-1)
          else
          	str = result.messages[0]
          message = "Please enter #{str}."
          @formError(message)
          return false
        postData =
          name: $("#name").val()
          phone: $("#phone").val()
          email: $("#email").val()
          message: $("#message").val()
          street: $("#street").val()
          city: $("#city").val()
          state: $("#state").val()
          zip: $("#zip").val()
          services: []
        for service in @added_services()
          postData.services.push ko.toJS _.find(@services(), (s) => s.name() == service)
        $.ajax
          type: "POST"
          url: "/send_mail"
          data: postData
          success: (data) =>
            clearForm()
            @succeeded(true)
            setTimeout ( =>
              @succeeded(false)
            ), 2000
      clearForm = () =>
        $("#name").val('')
        $("#phone").val('')
        $("#email").val('')
        $("#message").val('')
        $("#street").val('')
        $("#city").val('')
        $("#state").val('')
        $("#zip").val('')
        @selectedDropdown(new ServiceModel())
        @added_services([])
        ko.utils.arrayForEach(@services(), (service) ->
          service.selectedOptions([])    	  
        )
    	validateForm = () ->
        result = {}
        name = $("#name")
        email = $("#email")
        phone = $("#phone")
        message = $("#message")
        result = success: true, fields: [], messages: []

        if $.trim(name.val()).length == 0
          result.success = false
          result.fields.push name
          result.messages.push 'your name'
        if !email.formance('validate_email')
          result.success = false
          result.fields.push email
          result.messages.push 'a valid email address'
        if !phone.formance('validate_phone_number')
          result.success = false
          result.fields.push phone
          result.messages.push 'a valid phone number'
        if $.trim(message.val()).length == 0
          result.success = false
          result.fields.push message
          result.messages.push 'a message to send us'
        return result
  class ServiceModel
    constructor: (service = {}) ->
      @selectedOptions = ko.observableArray()
      @selectedOption = ko.observable(service.selectedOption)
      @id = ko.observable()
      @included = ko.observableArray()
      @options = ko.observableArray()
      @name =  ko.observable(service.name)
      @usesRadios = ko.observable(service.usesRadios || false)
      @id(service.id)
      @included(service.included)
      @options(service.options)
      
      if service.selectedOption
      	@selectedOptions([@selectedOption()])
      @selectedOption.subscribe (option) =>
      	@selectedOptions(_.reject(@selectedOptions, (opt)-> option == opt))
      	@selectedOptions.push(option)
      	
  viewModel = new ServicesViewModel()
  viewModel.isSelected = ((name) ->
    if typeof @selectedDropdown() == 'undefined' then return false
    selected = @selectedDropdown().name()
    selected == name
  ).bind(viewModel)
  ko.applyBindings(viewModel)