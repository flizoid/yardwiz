express = require('express')
routes = require('./routes')
http = require('http')
path = require('path')
compass = require('node-compass')
connect = require('connect-assets')
nodemailer = require('nodemailer')
mail = require('./routes/mail')
jsPaths = require "connect-assets-jspaths"

app = express()

# all environments
app.configure ->
	app.set 'port', process.env.PORT || 3001
	app.set 'views', path.join(__dirname, 'views')
	app.set 'view engine', 'jade'
	app.use compass project: path.join(__dirname, 'public')
	app.use express.static(path.join(__dirname, 'public'))
	app.use express.favicon()
	app.use express.logger('dev')
	app.use express.bodyParser()
	app.use express.methodOverride()
	app.use app.router


# development only
app.configure "development", ->
  app.use connect src: path.join(__dirname, 'public'), build: true
  global.js.root = 'javascripts'
  jsPaths(connect, console.log)
  app.use express.errorHandler()


app.get '/', routes.index
app.post '/send_mail', mail.sendMail

http.createServer(app).listen app.get('port'), ->
  console.log('Express server listening on port ' + app.get('port'));