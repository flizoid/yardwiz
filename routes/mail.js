/**
 * New node file
 */

var mail = require("nodemailer").mail;
var util = require("util");

var buildHtml = function (data) {
	var detailsTemp = '\
		<h1>YardWiz Inquiry</h1> \
		<hr/>\
		<h3 style="margin-bottom: -10px">Customer Information</h3>\
		<p style="margin-bottom: -10px">Name: %s</p>\
		<p style="margin-bottom: -10px">Email Address: %s</p>\
		<p style="margin-bottom: -10px">Phone Number: %s</p>\
		<h3 style="margin-bottom: -10px">Address</h3>\
		<p style="margin-bottom: -10px">Street Name: %s</p>\
		<p style="margin-bottom: -10px">City: %s</p>\
		<p style="margin-bottom: -10px">State: %s</p>\
		<p style="margin-bottom: -10px">Zipcode: %d</p>\
		<h3 style="margin-bottom: -10px">Services</h3>';
	var serviceTemp = '\
		<p style="margin-bottom: -20px">Service Name: %s</p>\
		<h4 style="margin-bottom: -10px">Options for %s:</h4>\
		<ul>%s</ul>';
	var listTemp = '<li>%s</li>';
	var servicesStr = '';
	if(typeof(data.services) === 'undefined') {
		servicesStr = 'No Services Selected.';
	} else {
		data.services.forEach(function(service) {
			var serOpts = '';
			if(typeof(service.selectedOptions) === 'undefined') {
				serOpts += 'None Selected.';
			} else {
				service.selectedOptions.forEach(function(opt){
					serOpts += util.format(listTemp, opt);
				});
			}
			servicesStr += util.format(serviceTemp, service.name, service.name, serOpts);
		});
	}
	
	var ret = util.format(detailsTemp, data.name, data.email, data.phone, data.street, data.city, data.state, data.zip);
	ret += servicesStr;
	msgString = '<h3 style="margin-bottom: -10px">Message</h3><hr/><p>' + data.message + '</p>';
	ret += msgString;
	return ret;
};

var buildText = function(data) {
	var detailsTemp = '\
		YardWiz Inquiry\n\n\
		Customer Information\n\
		Name: %s\n\
		Email Address: %s\n\
		Phone Number: %s\n\
		Address\n\
		Street Name: %s\n\
		City: %s\n\
		State: %s\n\
		Zipcode: %d\n\n\
		Services\n\n';
	var serviceTemp = '\
		Service Name: %s\n\
		Options for %s:\n\
		%s\n';
	var listTemp = '%s\n';
	var servicesStr = '';
	if(typeof(data.services) === 'undefined') {
		servicesStr = 'No Services Selected.';
	} else {
		data.services.forEach(function(service) {
			var serOpts = '';
			if(typeof(service.selectedOptions) === 'undefined') {
				serOpts += 'None Selected.';
			} else {
				service.selectedOptions.forEach(function(opt){
					serOpts += util.format(listTemp, opt);
				});
			}
			servicesStr += util.format(serviceTemp, service.name, service.name, serOpts);
		});
	}
	
	var ret = util.format(detailsTemp, data.name, data.email, data.phone, data.street, data.city, data.state, data.zip);
	ret += servicesStr;
	msgString = 'Message\n\n' + data.message;
	ret += msgString;
	return ret;	
};

exports.sendMail = function(req, res, next){
	var name = req.body.name;
	mail({
	    from: name + " <yardwiznet@gmail.com>",
	    to: "yardwiznet@gmail.com", // list of receivers
	    subject: "YardWiz Service Inquiry", // Subject line
	    text: buildText(req.body), // plaintext body
	    html: buildHtml(req.body) // html body
	});
	res.send(200);
};

